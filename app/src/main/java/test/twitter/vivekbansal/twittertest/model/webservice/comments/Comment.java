package test.twitter.vivekbansal.twittertest.model.webservice.comments;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import test.twitter.vivekbansal.twittertest.model.webservice.User.User;
import test.twitter.vivekbansal.twittertest.view.utilities.Fields;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class Comment {

    private int id;
    private String body;
    private User mUser;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        this.mUser = user;
    }

    public void fromJSON(JSONObject json) throws JSONException, ParseException {
        id = json.optInt(Fields.Comment.KEY_ID, 0);
        body = json.optString(Fields.Comment.KEY_BODY, "");

        mUser = new User();
        mUser.fromJSON(json.optJSONObject(Fields.Comment.KEY_USER));

    }


}
