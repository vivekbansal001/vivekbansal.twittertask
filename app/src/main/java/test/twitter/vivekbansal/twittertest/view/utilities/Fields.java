package test.twitter.vivekbansal.twittertest.view.utilities;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class Fields {

    public static final int CONNECTION_TIMEOUT = 1000 * 20;
    public static final int READ_TIMEOUT = 1000 * 20;


    public class Issue {

        public static final String URL_ISSUES = "https://api.github.com/repos/crashlytics/secureudid/issues";

        public static final String KEY_ID = "id";
        public static final String KEY_TITLE = "title";
        public static final String KEY_BODY = "body";
        public static final String KEY_UPDATED_AT = "updated_at";
        public static final String KEY_COMMENTS = "comments";
        public static final String KEY_COMMENTS_URL = "comments_url";

        public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    }


    public class Comment {

        public static final String KEY_ID = "id";
        public static final String KEY_BODY = "body";
        public static final String KEY_USER = "user";

        public class User {
            public static final String KEY_LOGIN = "login";
            public static final String KEY_ID = "id";
        }

    }


}
