package test.twitter.vivekbansal.twittertest.model.webservice.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import test.twitter.vivekbansal.twittertest.view.utilities.Fields;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class User {

    private int id;
    private String login;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void fromJSON(JSONObject json) throws JSONException, ParseException {
        id = json.optInt(Fields.Comment.User.KEY_ID, 0);
        login = json.optString(Fields.Comment.User.KEY_LOGIN, "");
    }


}
