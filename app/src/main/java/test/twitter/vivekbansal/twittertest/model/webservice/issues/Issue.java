package test.twitter.vivekbansal.twittertest.model.webservice.issues;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import test.twitter.vivekbansal.twittertest.view.utilities.Fields;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class Issue implements Comparable<Issue> {

    private int id;
    private String title;
    private String body;
    private String updatedAt;
    private Date updatedAtDate;
    private int comments;
    private String commentsUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updated_at) {
        this.updatedAt = updated_at;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String comments_url) {
        this.commentsUrl = comments_url;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public Date getUpdatedAtDate() {
        return updatedAtDate;
    }

    public void setUpdatedAtDate(Date updatedAtDate) {
        this.updatedAtDate = updatedAtDate;
    }

    public void fromJSON(JSONObject json) throws JSONException, ParseException {
        id = json.optInt(Fields.Issue.KEY_ID, 0);
        title = json.optString(Fields.Issue.KEY_TITLE, "");
        body = json.optString(Fields.Issue.KEY_BODY, "");
        updatedAt = json.optString(Fields.Issue.KEY_UPDATED_AT, "");
        comments = json.optInt(Fields.Issue.KEY_COMMENTS, 0);
        commentsUrl = json.optString(Fields.Issue.KEY_COMMENTS_URL, "");

        SimpleDateFormat formatter = new SimpleDateFormat(Fields.Issue.DATE_FORMAT);
        try {
            updatedAtDate = formatter.parse(updatedAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int compareTo(Issue another) {

        Date anotherDate = another.getUpdatedAtDate();

        if (updatedAtDate.getTime() > anotherDate.getTime())
            return 1;
        else
            return -1;
    }
}
