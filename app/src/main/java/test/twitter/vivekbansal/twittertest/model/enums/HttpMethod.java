package test.twitter.vivekbansal.twittertest.model.enums;

/**
 * Created by vivekbansal on 17/03/16.
 */
public enum HttpMethod {

    GET("Get"), POST("Post");

    private String methodName;

    HttpMethod(String methodName) {
        this.methodName = methodName;
    }

    public static String getRequestSerial(HttpMethod method) {
        return method.methodName;
    }

}
