package test.twitter.vivekbansal.twittertest.view.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import test.twitter.vivekbansal.twittertest.R;
import test.twitter.vivekbansal.twittertest.model.webservice.comments.CommentList;

/**
 * Created by vivekbansal on 18/03/16.
 */
public class CommentAdapter extends BaseAdapter {

    private Context mContext;
    private CommentList mCommentList;
    private LayoutInflater mLayoutInflater;

    public CommentAdapter(Context context, CommentList commentList) {
        mContext = context;
        mCommentList = commentList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mCommentList.getCommentList().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView = mLayoutInflater.inflate(R.layout.comment_item, null);

        holder.username = (TextView) rowView.findViewById(R.id.tv_comment_username);
        holder.desc = (TextView) rowView.findViewById(R.id.tv_comment_desc);

        holder.username.setText(mCommentList.getCommentList().get(position).getUser().getLogin());
        holder.desc.setText(mCommentList.getCommentList().get(position).getBody());

        return rowView;
    }

    public class Holder {
        TextView username;
        TextView desc;
    }

}
