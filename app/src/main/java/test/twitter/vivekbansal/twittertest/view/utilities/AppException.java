package test.twitter.vivekbansal.twittertest.view.utilities;

/**
 * Created by vivekbansal on 18/03/2016
 */
public class AppException extends Exception {

    private static int exceptionCode;
    private static String exceptionMessage;
    private static AppException appException;

    //Make Singleton
    private AppException() {
        //Do Nothing...
    }

    //Get Instance
    public static AppException getInstance(int code, String message) {

        appException = new AppException();

        //Set Exception Code
        exceptionCode = code;

        //Set Exception Message
        exceptionMessage = message;

        return appException;
    }

    public int getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
