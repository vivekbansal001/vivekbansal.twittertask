package test.twitter.vivekbansal.twittertest.view.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import test.twitter.vivekbansal.twittertest.R;
import test.twitter.vivekbansal.twittertest.model.listeners.CommentListener;
import test.twitter.vivekbansal.twittertest.model.webservice.issues.IssueList;

/**
 * Created by vivekbansal on 18/03/16.
 */
public class IssueAdapter extends BaseAdapter {

    private Context mContext;
    private IssueList mIssueList;
    private LayoutInflater mLayoutInflater;
    private CommentListener mCommentListener;

    public IssueAdapter(Context context, IssueList issueList, CommentListener commentListener) {
        mContext = context;
        mIssueList = issueList;
        mCommentListener = commentListener;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mIssueList.getIssueList().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Holder holder = new Holder();
        View rowView = mLayoutInflater.inflate(R.layout.issue_item, null);

        holder.title = (TextView) rowView.findViewById(R.id.tv_issue_title);
        holder.desc = (TextView) rowView.findViewById(R.id.tv_issue_desc);

        holder.title.setText(mIssueList.getIssueList().get(position).getTitle());
        holder.desc.setText(mIssueList.getIssueList().get(position).getBody());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCommentListener.showComments(position);
            }
        });

        return rowView;
    }

    public class Holder {
        TextView title;
        TextView desc;
    }

}
