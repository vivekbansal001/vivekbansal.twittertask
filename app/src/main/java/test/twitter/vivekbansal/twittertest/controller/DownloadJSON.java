package test.twitter.vivekbansal.twittertest.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import test.twitter.vivekbansal.twittertest.model.enums.HttpMethod;
import test.twitter.vivekbansal.twittertest.model.listeners.ResponseListener;
import test.twitter.vivekbansal.twittertest.view.utilities.AppConstants;
import test.twitter.vivekbansal.twittertest.view.utilities.AppException;
import test.twitter.vivekbansal.twittertest.view.utilities.Fields;

/**
 * Created by vivekbansal on 17/03/16.
 */

public class DownloadJSON extends AsyncTask<String, Integer, String> {

    private Context mContext;
    private HttpMethod mHttpMethod;
    private ResponseListener mResponseListener;

    public DownloadJSON(Context context, HttpMethod httpMethod, ResponseListener responseListener) {
        mContext = context;
        mHttpMethod = httpMethod;
        mResponseListener = responseListener;
    }

    @Override
    protected String doInBackground(String... urls) {

        try {
            return downloadUrl(urls[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (TextUtils.isEmpty(result)) {
            AppException appException = AppException.getInstance(AppConstants.ERROR_CODE_101, result);
            mResponseListener.onFailureResponse(appException);
        } else {
            mResponseListener.onSuccessResponse(result);
        }
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(Fields.READ_TIMEOUT);
            conn.setConnectTimeout(Fields.CONNECTION_TIMEOUT);
            conn.setRequestMethod(mHttpMethod.name());
            conn.setDoInput(true);

            // Starts the query
            conn.connect();

            int response = conn.getResponseCode();
            if (response == 200 || response == 201) {

                is = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = convertInputStreamToString(is);

                return contentAsString;
            } else
                return null;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }


    private String convertInputStreamToString(InputStream inputStream) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        return result;
    }
}


