package test.twitter.vivekbansal.twittertest.model.webservice.issues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class IssueList {

    List<Issue> issueList = new ArrayList<Issue>();

    public List<Issue> getIssueList() {
        return issueList;
    }

    public void fromJSON(String jsonString) throws JSONException, ParseException {

        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Issue issue = new Issue();
            issue.fromJSON(jsonObject);

            issueList.add(issue);
        }
    }

}
