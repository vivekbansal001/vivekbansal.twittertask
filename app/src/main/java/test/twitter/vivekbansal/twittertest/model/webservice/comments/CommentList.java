package test.twitter.vivekbansal.twittertest.model.webservice.comments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivekbansal on 17/03/16.
 */
public class CommentList {

    List<Comment> commentList = new ArrayList<Comment>();

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void fromJSON(String jsonString) throws JSONException, ParseException {

        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Comment comment = new Comment();
            comment.fromJSON(jsonObject);

            commentList.add(comment);
        }
    }


}
