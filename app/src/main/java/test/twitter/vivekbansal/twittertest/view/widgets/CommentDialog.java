package test.twitter.vivekbansal.twittertest.view.widgets;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONException;

import java.text.ParseException;

import test.twitter.vivekbansal.twittertest.R;
import test.twitter.vivekbansal.twittertest.controller.DownloadJSON;
import test.twitter.vivekbansal.twittertest.model.enums.HttpMethod;
import test.twitter.vivekbansal.twittertest.model.listeners.ResponseListener;
import test.twitter.vivekbansal.twittertest.model.webservice.comments.CommentList;
import test.twitter.vivekbansal.twittertest.view.utilities.AppException;
import test.twitter.vivekbansal.twittertest.view.utilities.ProgressWheel;
import test.twitter.vivekbansal.twittertest.view.utilities.Util;

/**
 * Created by vivekbansal on 18/03/16.
 */
public class CommentDialog extends DialogFragment {

    private static final String ARG_URL = "comment_fetch_url";
    private static Context context;

    ListView mListView;
    ProgressWheel mProgressWheel;

    private String commentUrl;
    private CommentList mCommentList;
    private CommentAdapter mCommentAdapter;

    public CommentDialog() {
    }

    public static CommentDialog getInstance(Context context, String url) {

        CommentDialog.context = context;

        CommentDialog commentDialog = new CommentDialog();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_URL, url);
        commentDialog.setArguments(bundle);

        return commentDialog;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            commentUrl = getArguments().getString(ARG_URL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().requestFeature(STYLE_NO_TITLE);

        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.white);
        View view = inflater.inflate(R.layout.dialog_comment, container);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListView = (ListView) getView().findViewById(R.id.comment_listview);
        mProgressWheel = (ProgressWheel) getView().findViewById(R.id.progress_wheel_complete_screen_comment);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        getData();

    }

    private void getData() {

        if (!Util.haveNetworkConnection(context)) {
            showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
            return;
        }

        mProgressWheel.setVisibility(View.VISIBLE);

        new DownloadJSON(context, HttpMethod.GET, new ResponseListener() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    mCommentList = new CommentList();
                    mCommentList.fromJSON(response);

                    mCommentAdapter = new CommentAdapter(context, mCommentList);
                    mListView.setAdapter(mCommentAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } catch (ParseException e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } catch (Exception e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } finally {
                    mProgressWheel.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailureResponse(AppException e) {
                showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                mProgressWheel.setVisibility(View.GONE);
            }
        }).execute(commentUrl);

    }


    private void showAlertDialog(String title, String desc) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(title);
        alertDialog.setMessage(desc);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getData();
                dialog.cancel();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                getDialog().dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() != null)
            getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation);
    }
}

