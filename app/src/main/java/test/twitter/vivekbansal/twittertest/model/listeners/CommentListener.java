package test.twitter.vivekbansal.twittertest.model.listeners;

/**
 * Created by vivekbansal on 18/03/16.
 */
public interface CommentListener {

    void showComments(int position);

}
