package test.twitter.vivekbansal.twittertest.view.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by vivekbansal on 15/01/15.
 */
public class Util {

    final public static boolean IS_DEVELOPMENT = true;
    final private static String LOG_TAG = "TwitterTest";

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        if (context == null)
            return false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        for (NetworkInfo ni : cm.getAllNetworkInfo()) {
            if (ni.getType() == ConnectivityManager.TYPE_WIFI && ni.isConnected())
                haveConnectedWifi = true;
            if (ni.getType() == ConnectivityManager.TYPE_MOBILE && ni.isConnected())
                haveConnectedMobile = true;
        }

        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static final boolean hasHoneycombMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    public static void v(String msg) {
        if (IS_DEVELOPMENT) {
            Log.v(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void i(String msg) {
        if (IS_DEVELOPMENT) {
            Log.i(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void w(String msg) {
        if (IS_DEVELOPMENT) {
            Log.w(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void d(String msg) {
        if (IS_DEVELOPMENT) {
            Log.d(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
        }
    }

    public static void e(String msg) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg);
    }

    public static void e(String msg, Exception e) {
        Log.e(LOG_TAG, TextUtils.isEmpty(msg) ? "null" : msg, e);
    }


}
