package test.twitter.vivekbansal.twittertest.model.listeners;

import test.twitter.vivekbansal.twittertest.view.utilities.AppException;

public interface ResponseListener {

    public void onSuccessResponse(String response);

    public void onFailureResponse(AppException e);

}
