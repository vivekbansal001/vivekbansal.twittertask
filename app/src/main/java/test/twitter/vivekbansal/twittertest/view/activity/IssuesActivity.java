package test.twitter.vivekbansal.twittertest.view.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import org.json.JSONException;

import java.text.ParseException;
import java.util.Collections;

import test.twitter.vivekbansal.twittertest.R;
import test.twitter.vivekbansal.twittertest.controller.DownloadJSON;
import test.twitter.vivekbansal.twittertest.model.enums.HttpMethod;
import test.twitter.vivekbansal.twittertest.model.listeners.CommentListener;
import test.twitter.vivekbansal.twittertest.model.listeners.ResponseListener;
import test.twitter.vivekbansal.twittertest.model.webservice.issues.IssueList;
import test.twitter.vivekbansal.twittertest.view.utilities.AppException;
import test.twitter.vivekbansal.twittertest.view.utilities.Fields;
import test.twitter.vivekbansal.twittertest.view.utilities.ProgressWheel;
import test.twitter.vivekbansal.twittertest.view.utilities.Util;
import test.twitter.vivekbansal.twittertest.view.widgets.CommentDialog;
import test.twitter.vivekbansal.twittertest.view.widgets.IssueAdapter;

public class IssuesActivity extends AppCompatActivity implements CommentListener {

    private IssueList mIssueList;

    private ListView mListView;
    private IssueAdapter mIssueAdapter;
    private ProgressWheel mProgressWheel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues);

        mListView = (ListView) findViewById(R.id.listView);
        mProgressWheel = (ProgressWheel) findViewById(R.id.progress_wheel_complete_screen);
        mProgressWheel.setBarColor(getResources().getColor(R.color.colorPrimary));
        mProgressWheel.setProgress(0.0f);
        mProgressWheel.spin();

        getData();
    }


    private void getData() {

        if (!Util.haveNetworkConnection(this)) {
            showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
            return;
        }

        mProgressWheel.setVisibility(View.VISIBLE);

        new DownloadJSON(getApplicationContext(), HttpMethod.GET, new ResponseListener() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    mIssueList = new IssueList();
                    mIssueList.fromJSON(response);

                    Collections.sort(mIssueList.getIssueList());

                    mIssueAdapter = new IssueAdapter(IssuesActivity.this, mIssueList, IssuesActivity.this);
                    mListView.setAdapter(mIssueAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } catch (ParseException e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } catch (Exception e) {
                    e.printStackTrace();
                    showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                } finally {
                    mProgressWheel.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailureResponse(AppException e) {
                showAlertDialog(getString(R.string.no_network_title), getString(R.string.no_network_desc));
                mProgressWheel.setVisibility(View.GONE);
            }
        }).execute(Fields.Issue.URL_ISSUES);

    }


    private void showAlertDialog(String title, String desc) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(title);
        alertDialog.setMessage(desc);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getData();
                dialog.cancel();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                IssuesActivity.this.finish();
            }
        });

        alertDialog.show();
    }


    @Override
    public void showComments(int position) {

        CommentDialog commentDialog = CommentDialog.getInstance(this, mIssueList.getIssueList().get(position).getCommentsUrl());
        commentDialog.show(getSupportFragmentManager(), CommentDialog.class.getName());

    }


}
